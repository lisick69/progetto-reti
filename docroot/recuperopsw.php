<?php
//recuperapsw.php
include 'connetti.php';
include 'header.php';
 
//controlla se utente gia loggato , se si non visualizza la pagina
if(isset($_SESSION['signed_in']) && $_SESSION['signed_in'] == true)
{
    echo 'Sei già connesso al forum, puoi <a href="logout.php"> uscire </a> se vuoi.';
}
else
{
    if($_SERVER['REQUEST_METHOD'] != 'POST')
    {
        /*form non postato, visualizzato*/
        echo '
        <section id="content">
	   <article class="col2 pad_left1">
        <h2>Recupero Password</h2>
        <form method="post" id="ContactForm" action="">
        <table >
			<tr><th colspan=2>Richiedi password</th></tr>
            <tr><td>Username:</td><td><input type="text" name="username" /></td></tr>
            <tr><td>E-mail:</td><td><input type="email" name="email"></td></tr>
            <tr><td ></td><td><input type="submit" value="Invia richiesta" class="button" /></td>
         </table>
         </form>
         </article>
         </section>';
    }
    else
    {
   	 /* la form è stata postata , 3 passaggi:
    	1.  verifica dati
        2.  darà la possibilità all 'utente di correggere campi
        3.  salvare i dati
    */
    	$errori = array(); /* array errori */
         
        if(isset($_POST['username']) && isset($_POST['email']))
        {
        	$sql='
            	SELECT COUNT(*) as n 
                FROM utenti 
                WHERE username = "'. mysql_real_escape_string($_POST['username']) .'"
           		AND email = "'. mysql_real_escape_string($_POST['email']) .'"';
        	$result = mysql_query($sql);
        	$arr=mysql_fetch_array($result);
        	$num=$arr['n'];
       		if($num!=1)
        	{
            	$errors[] = 'Combinazione utente e email non corretta , riprovare !';
        	}
        }
        else $errors[] = 'Il campo username e password non possono essere vuoti !.';
         
        if(!empty($errors)) /*controlla array vuoto, se ci sono errori sono nell array ()*/
        {
            echo 'Ops..alcuni campi presentano errori..';
            echo '<ul>';
        	foreach($errors as $key => $value) /* scorre l'array e visualizza errori */
        	{
            	echo '<li>' . $value . '</li>'; /* genera lista di errori */
        	}
        	echo '</ul>';
            echo '<a href="index.php">Vai alla Home </a>.';
        }
        else
        {
        	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    		$psw = substr( str_shuffle( $chars ), 0, 8 );            
            $to = mysql_real_escape_string($_POST['email']);
			$header = "FROM: Around The World <http://appsalvagente.altervista.org/>\r\n";
			$message = "Ci è arrivata una richiesta di recupero password ,di seguito ti è riportata la nuova : ".$psw.", puoi ora accedere con la nuova password e modificarla tramite la pagina del profilo.";
			$subject = "Email Recupero Password";
        		
			//echo"<script>window.location.href = 'index.php';</script>";
             $sqledit='	UPDATE utenti
						SET password = "'. $psw .'"
						WHERE username = "'. mysql_real_escape_string($_POST['username']) .'"
           				AND email = "'. mysql_real_escape_string($_POST['email']) .'"';
            $resultedit = mysql_query($sqledit);
       		if(!$resultedit)
        	{
            	echo 'Operazione fallita, si prega di riprovare più tardi !';
                echo ' <a href="index.php">Vai alla home </a>.';
        	}
            else{
            	mail($to,$subject,$message,$header) or die();//password inviata con successo        
				echo 'Operazione andata a buon fine dovresti a brevere ricevere una mail con la nuova password<a href="index.php">.Torna alla home </a>.';
         	}
         }
    }
}

include 'footer.php';
?>