<?php
//index.php
include 'header.php';
include 'dbconfig.php';

if($_SERVER['REQUEST_METHOD'] != 'POST')
{
	echo '

	<section id="content">
	   <article class="col2 pad_left1">
        <h2>form Registrazione</h2>
        <form method="post" id="ContactForm" action="">
			<table>
				<tr><td>Nome:</hr></td><td><input type="text" class="input2" name="nome"></td></tr>
				<tr><td>Cognome:</hr></td><td><input type="text" class="input2" name="cognome"></td></tr>
				<tr><td>Username:</hr></td><td><input type="text" class="input2" name="username"></td></tr>
				<tr><td>Password:</td><td><input type="password" class="input2" name="password"></td></tr>
				<tr><td>Conferma Password:&nbsp</td><td><input type="password" class="input2" name="cpassword"></td></tr>
				<tr><td>e-mail:</td><td><input type="email" class="input2" name="email"></td></tr>				
				<tr><td></td><td><input type="submit" value="Send" class="button">
								<input type="reset" value="Clear" class="button">	</tr> 
			</table>
			</div>
			</form>
		</article>
	</section> 
	</header>
	  
  ';
}
else
{  
	 $errors = array(); /* array errori */
     
    if(isset($_POST['nome']))
    {
        //se esiste
        if(!ctype_alpha($_POST['nome']))
        {
            $errors[] = 'Il campo nome può contenere solo caratteri.';
        }
        if(strlen($_POST['nome']) > 30)
        {
            $errors[] = 'Il campo nome non può essere piu lungo di 30.';
        }
	}
    else
    {
        $errors[] = 'Il campo nome non può essere vuoto.';
    }
    
    if(isset($_POST['cognome']))
    {
        //se esiste
        if(!ctype_alpha($_POST['cognome']))
        {
            $errors[] = 'Il campo cognome può contenere solo caratteri.';
        }
        if(strlen($_POST['cognome']) > 60)
        {
            $errors[] = 'Il campo cognome non può essere piu lungo di 60.';
        }
    }
  	else
    {
        $errors[] = 'Il campo cognome non può essere vuoto.';
    }
    
    if(isset($_POST['email'])){
    //se esiste
		$query="SELECT email FROM utenti WHERE email='".$_POST['email']."'";
		$result = $connessione->query($query);
		if (!$result){
			echo "Errore della query: " . $connessione->error . ".";
			exit();
		}
		else{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			if($row)
				$errors[] = 'email già registrata!';
		}
	}
	else{
		$errors[] = "l'email non può essere vuota!!";
	}
    
    if(isset($_POST['username']))
    {
        //se esiste
		$query="SELECT username FROM utenti WHERE username='".$_POST['username']."'";
		$result = $connessione->query($query);
 		if (!$result) {
			echo "Errore della query: " . $connessione->error . ".";
			exit();
		}
		else{
			$row = $result->fetch_array(MYSQLI_ASSOC);
			if($row)
				 $errors[] = 'username già registrato!';
		}
		
        if(!ctype_alnum($_POST['username']))
        {
            $errors[] = 'Il campo username può contenere solo numeri o caratteri.';
        }
        if(strlen($_POST['username']) > 60)
        {
            $errors[] = 'Il campo username non può essere piu lungo di 60.';
        }
    }
   	else
    {
        $errors[] = 'Il campo username non può essere vuoto.';
    }
   
    if(isset($_POST['password']))
    {
        if($_POST['password'] != $_POST['cpassword'])
        {
            $errors[] = 'Le password inserite non corrispondono.';
        }
        if(strlen($_POST['password']) < 6)
        {
            $errors[] = 'Il campo password non può essere meno di 6 caratteri.';
        }
    }
    else
    {
        $errors[] = 'Il campo password non può essere vuoto.';
    }
	
	if(!empty($errors)) /*controlla array vuoto, se ci sono errori sono nell array ()*/
    {
	    echo 'Ops..alcuni campi presentano errori..';
        echo '<ul>';
        foreach($errors as $key => $value) /* scorre l'array e visualizza errori */
        {
            echo '<li>' . $value . '</li>'; /* genera lista di errori */
        }
        echo '</ul>';
    }
    else
    {
		
		$query="
			INSERT INTO utenti (nome, cognome,username,email,password) 
			VALUES ('" . mysql_real_escape_string($_POST['nome']) . "',
                	   '" . mysql_real_escape_string($_POST['cognome']) . "',
                       '" . mysql_real_escape_string($_POST['username']) . "',
					   '" . mysql_real_escape_string($_POST['email']) . "',
                       '" . mysql_real_escape_string($_POST['password']) . "')";
		
		if (!$connessione->query($query)) 
		{
			echo "Errore della query: " . $connessione->error . ".";
		}
		else
		{
			echo "Registrazione avvenuta con successo.";
		}
	}
}
include 'footer.php';
  
	 