<?php
//index.php
include 'header.php';
echo '
  <article class="col1">
        <img src="images/travel_2.jpg" alt="">

      </article>
      <article class="col1 pad_left1">
        <div class="text"> <img src="images/logo_atw.png" alt="">
          <h2>Enjoy</h2>
		  <p>Registrati se ancora non lo hai ancora fatto</p>
          <a href="registrazione.php" class="button">Register</a> </div>
      </article>
      <div class="img"><img src="images/img.jpg" alt=""></div>
    </header>
    <section id="content">
      <article class="col1">
	    <h3>Hot Travel</h3>
        <div class="pad">
          <div class="wrapper under">
            <figure class="left marg_right1"><img src="images/page1_img1.jpg" alt=""></figure>
            <p class="pad_bot2"><strong>Italy<br>
              Holidays</strong></p>
            <p class="pad_bot2">Visita Roma, la città eterna che non muore mai.</p>
            <a href="citta.php?nome=Roma" class="marker_1"></a> </div>
          <div class="wrapper under">
            <figure class="left marg_right1"><img src="images/page1_img2.jpg" alt=""></figure>
            <p class="pad_bot2"><strong>Philippines<br>
              Travel</strong></p>
            <p class="pad_bot2">Immergiti nelle bellezze del sud-est asiatico.</p>
            <a href="citta.php?nome=Filippine" class="marker_1"></a> </div>
          <div class="wrapper">
            <figure class="left marg_right1"><img src="images/page1_img3.jpg" alt=""></figure>
            <p class="pad_bot2"><strong>Cruise<br>
              Holidays</strong></p>
            <p class="pad_bot2">Sole, palme, mare e mojito.</p>
            <a href="citta.php?nome=Caraibi" class="marker_1"></a> </div>
        </div>
      </article>
      <article class="col2 pad_left1">
	  <br />
        <h2>Popular Places</h2>
        <div class="wrapper under">
          <figure class="left marg_right1"><img src="images/parigi.jpg" alt=""></figure>
          <p class="pad_bot2"><strong>Paris</strong></p>
          <p class="pad_bot2">Parigi è la capitale  della Francia ed  è una delle più belle e visitate città del mondo, densa di magia ed ispiratrice di emozioni e poesia, la Città delle luci, capace di sorprendere ed incantare i turisti che accorrono ogni anno per godere delle sue innumerevoli bellezze.</p>
          <p class="pad_bot2"><strong>Tour Eiffel</strong> La "Tour Eiffel" è chiamata così dal nome del suo progettista, l&#8217ingegnere Gustave Eiffel. La struttura fu costruita dal 1887 al 1889, in occasione dell&#8217"Exposition Universelle" con lo scopo di commemorare il centenario della Rivoluzione Francese.</p>
          <a href="citta.php?nome=Paris" class="marker_2"></a> </div>
        <div class="wrapper">
          <figure class="left marg_right1"><img src="images/londra.jpg" alt=""></figure>
          <p class="pad_bot2"><strong>London</strong></p>
          <p class="pad_bot2">Londra è la capitale del Regno Unito e dell&#8217Inghilterra e una delle città più grandi d&#8217Europa. E&#8217 una delle principali città che influenzano il resto del mondo in fatto di cultura, comunicazione, politica, economia e arte. Londra è la città più popolata dell&#8217Unione Europea.</p>
          <p class="pad_bot2"><strong>Big Ben</strong> Big Ben è il nome della campana principale del Grande Orologio di Westminster, sulla Clock Tower a Londra. Tradizionalmente viene chiamata Big Ben l&#8217intera torre dell&#8217orologio, alta 96.3 metri, costruita in stile neo-gotico.</p>
          <a href="citta.php?nome=London" class="marker_2"></a> </div>
      </article>
    </section>
  </div>
 
  
  ';
 include 'footer.php';
  