<?php
//registrazione.php
include 'connetti.php';
include 'header.php';

if (isset($_SESSION['signed_in'])) {
    
    //controllo username esistente
    $posts_sql    = "SELECT
		utenti.username,utenti.password
		FROM utenti
		WHERE username = '" . mysql_real_escape_string($_SESSION['username']) . "';";
    $posts_result = mysql_query($posts_sql);
   
    if ($posts_result) 
	{//se ok
   
        $posts_row = mysql_fetch_assoc($posts_result); //salvo username in caso di riuso
        
		if ($_SERVER['REQUEST_METHOD'] != 'POST') 
		{
            /*form non postato, visualizzato
            nota la action="" fa in modo che la form faccia post su stessa pagina */
            echo '
            <section id="content">
	   <article class="col2 pad_left1">
        <h2>Modifica dati</h2>
        N.B.: i campi lasciati in bianco non verranno modificati <br>
        <form method="post" id="ContactForm" action="" enctype="multipart/form-data">
            
					<table >
					
     	<tr><td>Nome Utente:</td><td><input type="text" name="nome" default="null"/></td></tr>
        <tr><td>Cognome Utente:</td><td><input type="text" name="cognome" /></td></tr>
        <tr><td>Nuova Password(min.6):</td><td><input type="password" name="password"/></td></tr>
        <tr><td>Ripeti Password:</td><td><input type="password" name="password_check"/></td></tr>
        <tr><td>E-mail:</td><td><input type="email" name="email"/></td></tr>
		
		<tr><td>Password(min.6, in caso di cambio la vecchia password):</td><td><input type="password" name="password_old"/></td></tr>
        <tr><td></td><td><input type="submit" value="Modifica" class="button"/></td></tr>
        </table>
			</form>
            </article>
	</section> ';
        } 
		else 
		{
        	
            /* la form è stata postata , 3 passaggi:
            1.  verifica dati
            2.  darà la possibilità all 'utente di correggere campi
            3.  salvare i dati
            */
            $errors = array();
            /* array errori */
            
            if (isset($_POST['nome']) && ($_POST['nome']!="")) 
			{
            //echo'nome settato';
                 //se esiste
                if (!ctype_alpha($_POST['nome'])) {
                    $errors[] = 'Il campo nome può contenere solo caratteri.';
                }
                if (strlen($_POST['nome']) > 30) {
                    $errors[] = 'Il campo nome non può essere piu lungo di 30.';
                }
            } 
            
            if (isset($_POST['cognome'])&& ($_POST['cognome']!="")) 
			{
            //echo'cognome settato';
                //se esiste
                if (!ctype_alpha($_POST['cognome'])) {
                    $errors[] = 'Il campo cognome può contenere solo caratteri.';
                }
                if (strlen($_POST['nome']) > 60) {
                    $errors[] = 'Il campo cognome non può essere piu lungo di 60.';
                }
            }
            
            
            if (isset($_POST['email'])&& ($_POST['email']!="")) 
			{
            //echo'email settato';
                //se esiste
                $sql    = 'SELECT COUNT(*) as n FROM utenti WHERE email = "' . mysql_real_escape_string($_POST['email']) . '"';
                $result = mysql_query($sql);
                $arr    = mysql_fetch_array($result);
                $num    = $arr['n'];
                if ($num != 0) {
                    $errors[] = 'Email già registrata !.';
                }
            }
            
            
            //rivedi
            if (isset($_POST['password'])&& ($_POST['password']!="")) {
            //echo'nuova pass settato';
                if ($_POST['password'] != $_POST['password_check']) {
                    $errors[] = 'Le password inserite non corrispondono.';
                }
                if (strlen($_POST['password']) < 6) {
                    $errors[] = 'Il campo username non può essere meno di 6 caratteri.';
                }
            }
            
            //rivedi
            if (!(isset($_POST['password'])&& isset($_POST['nome'])&& isset($_POST['cognome'])&& isset($_POST['email']))
			||(($_POST['password']=="") && ($_POST['nome']=="") && ($_POST['cognome']=="") &&( $_POST['email']==""))) 
			{
            
                $errors[] = 'Nessun campo settato/modificato torna alla home .<a href="index.php">Vai alla pagina home </a>.';
            }
			else
			{
				
				if (isset($_POST['password_old'])) //controlla se password c'è e uguale a utente
				{
               // $posts_row = mysql_fetch_assoc($posts_result);
               // echo "pass query :".$posts_row['utente_pass'];
               // echo " pass nuova ".$_POST['utente_pass_old'];
                if($posts_row['password']!=sha1($_POST['password_old']))
                    $errors[] = 'Password sbagliata !';
                
				}
			}

			
            
            //se effettivamente esiste...	
            $sql = "SELECT username
					FROM utenti
					WHERE username = '" . mysql_real_escape_string($_SESSION['username']) . "'
					AND password = '" . sha1($_POST['password_old']) . "'";
                   
            $result = mysql_query($sql);
            if ($result) 
			{//esiste  
				
                if (!empty($errors)) /*controlla array vuoto, se ci sono errori sono nell array ()*/ 
				{
                    
                    
                    echo 'Ops..alcuni campi presentano errori..';
                    echo '<ul>';
                    foreach ($errors as $key => $value) /* scorre l'array e visualizza errori */ 
					{
                        echo '<li>' . $value . '</li>';
                        /* genera lista di errori */
                    }
                    echo '</ul>';
                } 
				else 
				{
                    
                    if (isset($_POST['nome'])&&($_POST['cognome']!="")) 
					{
                        $sqledit    = '	UPDATE utenti
						SET nome = "' . mysql_real_escape_string($_POST['nome']) . '"
						WHERE username = "' . mysql_real_escape_string($_SESSION['username']).'"';
                        $resultedit = mysql_query($sqledit);
                        if (!$resultedit) {
                            echo 'Operazione fallita nom, si prega di riprovare più tardi ! <a href="index.php">Vai alla home </a>.';
                        }
                        //else echo'cambiato nom';
                    }
                    
                    if (isset($_POST['cognome'])&&($_POST['cognome']!="")) 
					{
                        $sqledit    = '	UPDATE utenti
						SET cognome = "' . mysql_real_escape_string($_POST['cognome']) . '"
						WHERE username = "' . mysql_real_escape_string($_SESSION['username']).'"';
                        $resultedit = mysql_query($sqledit);
                        if (!$resultedit) {
                            echo 'Operazione fallita cog, si prega di riprovare più tardi ! <a href="index.php">Vai alla home </a>.';
                        }
                       // else echo'cambiato cogn';
                    }
                    
                    if (isset($_POST['password'])&&($_POST['password']!="")) 
					{
                        $sqledit    = '	UPDATE utenti
						SET password = "' . sha1($_POST['password']) . '"
						WHERE username = "' . mysql_real_escape_string($_SESSION['username']).'"';
                        $resultedit = mysql_query($sqledit);
                        if (!$resultedit) {
                            echo 'Operazione fallita pass, si prega di riprovare più tardi ! <a href="index.php">Vai alla home </a>.';
                        }
                        //else echo'cambiata pass';
                    }
                    
                    if (isset($_POST['email'])&&($_POST['email']!="")) 
					{
                        $sqledit    = '	UPDATE utenti
						SET email = "' . mysql_real_escape_string($_POST['email']) . '"
						WHERE username = "' . mysql_real_escape_string($_SESSION['username']).'"';
                        $resultedit = mysql_query($sqledit);
                        if (!$resultedit) {
                            echo 'Operazione fallita ema, si prega di riprovare più tardi ! <a href="index.php">Vai alla home </a>.';
                        }
                        //else echo'cambiato user';
                    }
                    
                    echo 'Operazione avveuta con successo torna al <a href="profilo.php">profilo </a>.';
                }
            }
        }
    }
}
else{

	echo "sessione non sessionata";
}

include 'footer.php';

?>
