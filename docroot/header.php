<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
<?php 				session_start();  
					include_once("config.php");
					include_once("includes/functions.php");
					include "common.php";  
					include_once "fbconnect.php"; 
?>
<html lang="en">
<head>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1070533182980830',
      xfbml      : true,
      version    : 'v2.5'
    });
  };
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<title>Around the World</title>
<meta charset="utf-8">
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<script type="text/javascript" src="facebookSDK.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/ie6_script_other.js"></script>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
</head>

<body id="page1">
<!-- START PAGE SOURCE -->
<div class="extra">
  <div class="main">
    <header>
      <div class="wrapper">
        <h1><a href="index.php" id="logo">Around the World</a></h1>
        <div class="right">
          <div class="wrapper">
            <form id="search" action="#" method="post">
            </form>
          </div>
          <div class="wrapper">
            <nav>
              <ul>
                <li>
				
				<?php 
				if(isset($_SESSION['signed_in'])){
					echo '<a class="button" href="profilo.php"> '.$_SESSION['user'].' </a> <a class="button" href="logout.php">Log Out</a></li>';

                }
                else{
                	$loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$homeurl,'scope'=>$fbPermissions));
                    echo '<a class="buttonface" href="'.$loginUrl.'"> F Login </a> ';
					echo '<a class="button" href="registrazione.php"> Register</a>  <a class="button" href="login.php">Log In</a> </li>';
				}
                		$user = $facebook->getUser();
                        if ($user) {
 							 try{
                    			$user_profile = $facebook->api('/me?fields=id,first_name,last_name,email');
								$user = new Users();// nuova classe users    , sotto crea array con i dati che mi servono!!!
								//echo $user_profile['id'].$user_profile['first_name'].$user_profile['last_name'].$user_profile['email'];
								$user_data =$user->checkUser('facebook',$user_profile['id'],$user_profile['first_name'],$user_profile['last_name'],$user_profile['email']);
                    			//echo $user_data;
                		} 
                        catch (FacebookApiException $e) 
                        {
    							error_log($e);
                		}
				}
                ?>	
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <nav>
        <ul id="menu">
        <?php 
				if(isset($_SESSION['signed_in']))
					echo '<li><a href="bacheca.php" class="nav1">Home</a></li>
          <li><a href="about.php" class="nav2">About Us </a></li>
          <li><a href="tours.php" class="nav3">Our Tours </a></li>
          <li><a href="contacts.php" class="nav4">Contacts</a></li>
          <li class="end"><a href="help.php" class="nav5"> Help</a></li>
				';
				else
					echo '<li><a href="index.php" class="nav1">Home</a></li>
          <li><a href="about.php" class="nav2">About Us </a></li>
          <li><a href="tours.php" class="nav3">Our Tours </a></li>
          <li><a href="contacts.php" class="nav4">Contacts</a></li>
          <li class="end"><a href="help.php" class="nav5"> Help</a></li>
				';
				?>
        </ul>
      </nav>
