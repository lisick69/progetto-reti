<?php
//index.php
include 'header.php';
include 'dbconfig.php';

if(isset($_SESSION['signed_in'])){
	if( $_SESSION['signed_in'] == true)
	{
		unset($_SESSION['signed_in']);
		unset($_SESSION['id']);
		unset($_SESSION['user']);
		//var facebook login 
        include_once("config.php");
		$facebook->destroySession();
        unset($_SESSION['userdata']);
        header("Location:index.php");
	
	}
	else
	{
		echo 'Non sei loggato. Clicca per <a href="login.php">accedere</a>!';
	}
}
else
{
	echo 'Non sei loggato. Clicca per <a href="login.php">accedere</a>!';
}

include 'footer.php';
?>